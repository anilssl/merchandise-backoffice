package uk.co.tui.merchandisebackoffice.editors;

import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ListModelList;

import com.hybris.cockpitng.core.util.Validate;
import com.hybris.cockpitng.editors.EditorContext;
import com.hybris.cockpitng.editors.EditorListener;
import com.hybris.cockpitng.editors.impl.AbstractCockpitEditorRenderer;

import uk.co.tui.merchandisecore.model.SpotLightRecordModel;


/**
 * @author gkumar
 *
 *         Purpose: Allows to add only numerics to text box look like Int Box.
 *
 */
public class DefaultTextBoxEditor extends AbstractCockpitEditorRenderer<String>
{

	@Resource
	SessionService sessionService;

	@Override
	public void render(final Component parent, final EditorContext<String> context, final EditorListener<String> listener)
	{
		Validate.notNull("All parameters are mandatory", new Object[]
		{ parent, context, listener });
		// Create UI component
		final Combobox editorView = new Combobox();
		final ListModelList<String> lm2 = new ListModelList<String>(new ArrayList());
		editorView.setModel(lm2);

		editorView.addEventListener(Events.ON_SELECT, new EventListener()
		{

			@Override
			public void onEvent(final Event event) throws Exception
			{
				final Comboitem selected = editorView.getSelectedItem();
				final SpotLightRecordModel spot = (SpotLightRecordModel) parent.getAttribute("parentObject");
				spot.setName(selected.getValue().toString());
				listener.onValueChanged(selected.getValue().toString());
				if (Events.ON_SELECT.equals(event.getName()))
				{
					listener.onEditorEvent(EditorListener.ENTER_PRESSED);
				}

			}
		});

		// Handle events
		editorView.addEventListener(Events.ON_CLICK, new EventListener<Event>()
		{
			@Override
			public void onEvent(final Event event) throws Exception
			{
				handleEvent(editorView, event, listener, parent, context);
			}
		});
		editorView.addEventListener(Events.ON_OK, new EventListener<Event>()
		{
			@Override
			public void onEvent(final Event event) throws Exception
			{
				handleEvent(editorView, event, listener, parent, context);
			}
		});

		editorView.addEventListener(Events.ON_BLUR, new EventListener<Event>()
		{
			@Override
			public void onEvent(final Event event) throws Exception
			{
				handleEvent(editorView, event, listener, parent, context);
			}
		});

		// Add the UI component to the component tree
		editorView.setParent(parent);
	}

	/**
	 * Handle a view event on the editor view component.
	 *
	 * @param editorView
	 *           the view component
	 * @param event
	 *           the event to be handled
	 * @param listener
	 *           the editor listener to send change notifications to UI.
	 * @param parent
	 */
	protected void handleEvent(final Combobox editorView, final Event event, final EditorListener<String> listener,
			final Component parent, final EditorContext<String> context)
	{

		final SpotLightRecordModel spot = (SpotLightRecordModel) parent.getAttribute("parentObject");
		if (Objects.nonNull(spot.getCode()))
		{
			final List<String> indiaList = new ArrayList<String>();
			indiaList.add("KAR");
			indiaList.add("DEL");
			indiaList.add("KER");

			final List<SpotLightRecordModel> spotlights = sessionService.getAttribute("spotLightRecordsList");
			if (Objects.nonNull(spotlights))
			{
				indiaList.add("Session received object " + spotlights.size());
			}


			final List<String> usList = new ArrayList<String>();
			usList.add("NYC");
			usList.add("BRH");
			usList.add("CHC");


			switch (spot.getCode())
			{
				case "IND":
					final ListModelList<String> lm2 = new ListModelList<String>(indiaList);
					editorView.setModel(lm2);
					break;

				case "US":
					final ListModelList<String> lm3 = new ListModelList<String>(usList);
					editorView.setModel(lm3);
					break;
			}
		}


	}

}
