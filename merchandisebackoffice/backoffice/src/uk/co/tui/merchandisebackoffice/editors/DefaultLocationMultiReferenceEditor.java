package uk.co.tui.merchandisebackoffice.editors;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.zkoss.zk.ui.Component;

import com.hybris.cockpitng.editor.defaultmultireferenceeditor.DefaultMultiReferenceEditor;
import com.hybris.cockpitng.editors.EditorContext;
import com.hybris.cockpitng.editors.EditorListener;
import com.hybris.cockpitng.search.data.pageable.Pageable;
import com.hybris.cockpitng.search.data.pageable.PageableList;

import uk.co.tui.merchandisecore.model.AccommodationModel;


public class DefaultLocationMultiReferenceEditor<T> extends DefaultMultiReferenceEditor<T>
{

	public static final String MULTIREFEDITOR_COMPONENTID = "com.hybris.cockpitng.editor.defaultmultireferenceeditorx";
	@Resource
	FlexibleSearchService flexibleSearchService;

	@Resource
	ModelService modelService;

	@Override
	public void render(final Component parent, final EditorContext<Collection<T>> context,
			final EditorListener<Collection<T>> listener)
	{
		super.render(parent, context, listener);
		restrictAccommodationFacilities();
		addSocketInputEventListener("sampleSocket", createInputSocketEventListener());
		setComponentID(MULTIREFEDITOR_COMPONENTID);
	}

	private void restrictAccommodationFacilities()
	{

		final List<AccommodationModel> filLoc = new ArrayList<AccommodationModel>();

		final SearchResult<AccommodationModel> results = flexibleSearchService.search("Select {pk} from {Accommodation}");

		for (final AccommodationModel loc : results.getResult())
		{
			filLoc.add(loc);
		}
		pageable = (Pageable<T>) new PageableList<AccommodationModel>(filLoc, 3);
	}

}
