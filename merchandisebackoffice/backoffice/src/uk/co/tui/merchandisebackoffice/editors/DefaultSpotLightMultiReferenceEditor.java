package uk.co.tui.merchandisebackoffice.editors;

import java.util.Collection;
import javax.annotation.Resource;

import org.zkoss.zk.ui.Component;

import uk.co.tui.merchandisecore.model.SpotlightsModel;
import com.hybris.cockpitng.editor.defaultmultireferenceeditor.DefaultMultiReferenceEditor;
import com.hybris.cockpitng.editors.EditorContext;
import com.hybris.cockpitng.editors.EditorListener;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;

public class DefaultSpotLightMultiReferenceEditor<T> extends DefaultMultiReferenceEditor<T> { 
	
	public static final String MULTIREFEDITOR_COMPONENTID = "com.cockpitng.editor.multireference.defaultSpotLightList";
	  private static final int DEFAULT_PAGE_SIZE = 10;
	  
	  @Resource
	  FlexibleSearchService flexibleSearchService;
	  
	  @Resource
	  ModelService modelService;
	  
	  @Resource
	  SessionService sessionService;
	  
	@Override
	public void render(final Component parent,
			final EditorContext<Collection<T>> context,
			final EditorListener<Collection<T>> listener) {
		super.render(parent, context, listener);
		addSocketInputEventListener("sampleSocket",
		createInputSocketEventListener());
		setComponentID(MULTIREFEDITOR_COMPONENTID);
	    final Object parentComponent = parent.getAttribute(PARENT_OBJECT);
	    SpotlightsModel deals =  (SpotlightsModel) parentComponent;
	    sessionService.setAttribute("spotLightRecordsList", deals.getSpolightRecords());

	}


	
}
