/**
 *
 */
package uk.co.tui.merchandisebackoffice.widgets;


import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hybris.backoffice.navigation.NavigationNode;
import com.hybris.backoffice.widgets.advancedsearch.AbstractInitAdvancedSearchAdapter;
import com.hybris.backoffice.widgets.advancedsearch.impl.AdvancedSearchData;
import com.hybris.cockpitng.annotations.SocketEvent;
import com.hybris.cockpitng.core.config.impl.jaxb.hybris.advancedsearch.FieldType;
import com.hybris.cockpitng.search.data.ValueComparisonOperator;

import uk.co.tui.merchandisecore.enums.BrandType;


/**
 * @author anilkumar.reddy
 *
 */
public class MultiConditionSearchController extends AbstractInitAdvancedSearchAdapter
{
	public static final Logger LOG = LoggerFactory.getLogger(MultiConditionSearchController.class);

	private static final long serialVersionUID = 1L;

	private static final String BRAND = "brand";
	private static final String SOCKET_IN_NODE_SELECTED = "nodeSelected";
	private static String NODE_SELECTED = StringUtils.EMPTY;
	private static String BRAND_SELECTED = StringUtils.EMPTY;
	private static String PAGE_TYPE = StringUtils.EMPTY;

	@SocketEvent(socketId = SOCKET_IN_NODE_SELECTED)
	public void getNodeSelected(final NavigationNode navigationNode)
	{
		if (navigationNode != null)
		{
			NODE_SELECTED = navigationNode.getId();
			BRAND_SELECTED = getSelectedBrand(navigationNode);
			PAGE_TYPE = getPageType(navigationNode);
			LOG.info("Node selected: " + navigationNode.getId() + " | Parent: " + navigationNode.getParent().getId()
					+ " | Super Parent: " + navigationNode.getParent().getParent().getId() + " >> Brand : " + BRAND_SELECTED
					+ " | Page: " + PAGE_TYPE);
		}
	}


	/**
	 * @param navigationNode
	 * @return pageType
	 */
	private String getPageType(final NavigationNode navigationNode)
	{
		final String[] navNodeId = StringUtils.split(navigationNode.getId(), '_');
		return navNodeId[2];
	}


	/**
	 * @param navigationNode
	 * @return brand
	 */
	private String getSelectedBrand(final NavigationNode navigationNode)
	{
		final String[] navNodeId = StringUtils.split(navigationNode.getId(), '_');
		return navNodeId[1];
	}


	@SuppressWarnings("deprecation")
	@Override
	public void addSearchDataConditions(final AdvancedSearchData searchData)
	{
		//Deprecated method, do nothing
	}

	@Override
	public void addSearchDataConditions(final AdvancedSearchData searchData, final Optional<NavigationNode> navigationNode)
	{
		final FieldType brand = new FieldType();
		brand.setDisabled(Boolean.TRUE);
		brand.setSelected(Boolean.TRUE);
		brand.setName(BRAND);

		/*
		 * final SearchConditionData startsWithCondition = new SearchConditionData(brand, BrandType.TH,
		 * ValueComparisonOperator.EQUALS); searchData.addConditionList(ValueComparisonOperator.AND,
		 * Lists.newArrayList(startsWithCondition));
		 */
		searchData.addCondition(brand, ValueComparisonOperator.EQUALS, getBrand());
	}

	/**
	 * @return BrandType
	 */
	private Object getBrand()
	{
		switch (BRAND_SELECTED)
		{
			case "TH":
				return BrandType.TH;
			case "FC":
				return BrandType.FC;
			case "FJ":
				return BrandType.FJ;
			case "CR":
				return BrandType.CR;
		}
		return BrandType.TH;
	}


	@Override
	public String getNavigationNodeId()
	{
		return NODE_SELECTED;
	}

	@Override
	public String getTypeCode()
	{
		return PAGE_TYPE;
	}



}
