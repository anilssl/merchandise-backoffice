/**
 *
 */
package uk.co.tui.merchandisebackoffice.actions.clone;

import java.util.HashMap;
import java.util.Map;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;
import com.hybris.cockpitng.widgets.configurableflow.ConfigurableFlowContextParameterNames;

import uk.co.tui.merchandisecore.model.DealsPageModel;



/**
 * @author anilkumar.reddy
 *
 */
public class DealsPageCloneAction extends AbstractComponentWidgetAdapterAware implements CockpitAction<Object, Object>
{

	/*
	 * (non-Javadoc)
	 *
	 * @see com.hybris.cockpitng.actions.CockpitAction#perform(com.hybris.cockpitng.actions.ActionContext)
	 */
	@Override
	public ActionResult<Object> perform(final ActionContext<Object> ctx)
	{
		ActionResult<Object> result = null;
		if ((ctx.getData() instanceof DealsPageModel))
		{
			final DealsPageModel data = (DealsPageModel) ctx.getData();
			final Map<String, Object> contextMap = new HashMap<>();
			contextMap.put(ConfigurableFlowContextParameterNames.TYPE_CODE.getName(), "DealsPage");
			contextMap.put("configurableFlowConfigCtx", "clone-wizard");
			contextMap.put("uid", data.getUid());
			contextMap.put("name", data.getName());
			contextMap.put("brand", data.getBrand());
			contextMap.put("siteIds", data.getSiteIds());

			contextMap.put("globalConfiguration", data.getGlobalConfiguration());
			contextMap.put("merchandiseContents", data.getMerchandiseContents());
			contextMap.put("postProcessingConfiguration", data.getPostProcessingConfiguration());
			contextMap.put("generalFilters", data.getGeneralFilters());
			contextMap.put("aniteFacetCollection", data.getAniteFacetCollection());
			contextMap.put("destinationOptions", data.getDestinationOptions());

			sendOutput("cloneContext", contextMap);
			result = new ActionResult<>(ActionResult.SUCCESS, data);
		}
		else
		{
			result = new ActionResult<>(ActionResult.ERROR);
		}
		return result;
	}

}
