/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package uk.co.tui.merchandisecore.setup;

import static uk.co.tui.merchandisecore.constants.MerchandisecoreConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import uk.co.tui.merchandisecore.constants.MerchandisecoreConstants;
import uk.co.tui.merchandisecore.service.MerchandisecoreService;


@SystemSetup(extension = MerchandisecoreConstants.EXTENSIONNAME)
public class MerchandisecoreSystemSetup
{
	private final MerchandisecoreService merchandisecoreService;

	public MerchandisecoreSystemSetup(final MerchandisecoreService merchandisecoreService)
	{
		this.merchandisecoreService = merchandisecoreService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		merchandisecoreService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return MerchandisecoreSystemSetup.class.getResourceAsStream("/merchandisecore/sap-hybris-platform.png");
	}
}
